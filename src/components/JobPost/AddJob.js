import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { Container } from 'react-bootstrap';
import { getUser } from "../../store/reducers";


export const AddJob = () => {

    const dispatch = useDispatch();

    useEffect(() => {
      dispatch(getUser());
    }, [dispatch]);
  
    const user = useSelector((state) => state.user.user);
    console.log(user[1]);
  

    return (

        <Container fluid="md">
            <div className="card">
                <div className="card-header rounded">
                    <h4>Add New Job</h4>
                </div>
                <div className="card-body text-left">
                    <form>
                        <div className="row">
                            <div className="col-6">
                                <div className="form-group">
                                    <label htmlFor="inputName">Job Title</label>
                                    <input type="name" className="form-control" id="inputName" aria-describedby="textHelp" placeholder="Enter company name" />
                                </div>
                            </div>
                            <div className="col-6">
                                <div className="form-group">
                                    <label htmlFor="jobTypeId">Company</label>
                                    <select className="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                        <option value="">--Select--</option>
                                        <option value="1">Company One</option>
                                        <option value="0">Company Two</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-6">
                                <div className="form-group">
                                    <label htmlFor="expStart">Start Experience</label>
                                    <input type="number" className="form-control" id="expStart" placeholder="Enter start experience" />
                                </div>
                            </div>
                            <div className="col-6">
                                <div className="form-group">
                                    <label htmlFor="expEnd">End Experience</label>
                                    <input type="number" className="form-control" id="expEnd" placeholder="Enter end experience" />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-6">
                                <div className="form-group">
                                    <label htmlFor="payStart">Start Pay</label>
                                    <input type="number" className="form-control" id="payStart" placeholder="Enter start pay" />
                                </div>
                            </div>
                            <div className="col-6">
                                <div className="form-group">
                                    <label htmlFor="payEnd">End Pay</label>
                                    <input type="number" className="form-control" id="payEnd" placeholder="Enter end pay" />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="form-group">
                                    <label htmlFor="inputName">Description</label>
                                    <textarea type="text" className="form-control" id="textField" rows="4" aria-describedby="textHelp" placeholder="Enter job descriptions" />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-6">
                                <label htmlFor="jobTypeId">Status</label>
                                <select className="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                    <option value="">--Select--</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div><br />
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </Container >
    );
}
