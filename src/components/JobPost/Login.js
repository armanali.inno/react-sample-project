import React from "react";
import { Container } from "react-bootstrap"

const LoginPage = () => {
  return (
    <Container>
      <div className="card mb-3 mt-3 align-self-center">
        <div className="card-body text-left">
          <div className="col-sm-6 align-self-center">
            <form>
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" />
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default LoginPage;