import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { getUser, getJobPost } from "../../store/reducers";

import 'bootstrap/dist/css/bootstrap.min.css';
import DataTable from 'react-data-table-component';

// const data = [{ id: 1, title: 'Conan the Barbarian', company: 'Vipro India Pvt Ltd', expStart: 2, expEnd: 4, payStart: 12000, payEnd: 20000 }];
const columns = [
    {
        name: 'Job Title',
        selector: 'title',
        sortable: true,
    },
    {
        name: 'Company',
        selector: 'company',
        sortable: true,
        right: true,
    },
    {
        name: 'ExpStart',
        selector: 'expStart',
        sortable: true,
        right: true,
    },

    {
        name: 'ExpEnd',
        selector: 'expEnd',
        sortable: true,
        right: true,
    },

    {
        name: 'Start Pay',
        selector: 'payStart',
        sortable: true,
        right: true,
    },

    {
        name: 'End Pay',
        selector: 'payEnd',
        sortable: true,
        right: true,
    },
    {
        name: 'Status',
        selector: 'status',
        sortable: false,
        right: true,
    },
    {
        name: 'Action',
        selector: 'action',
        sortable: false,
        right: true,
    },

];


export const JobList = () => {
    const dispatch = useDispatch();


    useEffect(() => {
        dispatch(getJobPost());
    }, [dispatch]);


    const jobPost = useSelector((state) => state.user.jobPost);
    console.log(jobPost)

    return (
        <div className="wrapper ">
            <div className="main-panel">
                <div className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="card">
                                    <div className="card-header card-header-primary">
                                        <h4 className="card-title ">Job List</h4>
                                    </div>
                                    <div className="card-body">
                                        <DataTable
                                            columns={columns}
                                            data={jobPost}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12">
                            </div>
                        </div>
                    </div>
                    <footer className="footer">
                        <div className="container-fluid">
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    );
}
