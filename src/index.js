import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import App from './App';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {store} from './store/configureStore';

function Root() {

return (
    <div className="App">
      <Provider store={store}>
        <App />
      </Provider>
    </div>
  );
}

ReactDOM.render( <Root />,  document.getElementById('root'));

reportWebVitals();
