import React, { useEffect } from "react";
import { Router } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getUser } from "./store/reducers";
import history from "./routes/History";
// import { AddJob } from "./components/JobPost/AddJob";
import { JobList } from "./components/JobPost/JobPostList";
// import LoginPage from "./components/JobPost/Login";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);

  const user = useSelector((state) => state.user.user);
  console.log(user);

  return (
    <Router history={history}>
      <div className="App">
        {user && <h1> Hello, {user[1].userName} </h1>}
      </div>
      {/* <LoginPage /> */}
      {/* <AddJob /> */}
      <JobList />
      {/* {<Routes lang={lang} />} */}
    </Router>

  );
}

export default App;
