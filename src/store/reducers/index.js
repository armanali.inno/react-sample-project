export const GET_USER = "GET_USER";
export const SET_USER = "SET_USER";
export const GET_JOBPOST = "GET_JOBPOST";
export const SET_JOBPOST = "GET_JOBPOST";


export const getUser = () => ({
  type: GET_USER
});

export const setUser = (user) => ({
  type: SET_USER,
  user
});

export const getJobPost = () => ({
  type: GET_JOBPOST
});

export const setJobPost = (jobPost) => ({
  type: SET_JOBPOST,
  jobPost
});


const initialState = {
  loading: false,
  jobpost: undefined,
  user: undefined
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      const { user } = action;
      return { ...state, user };
    case SET_JOBPOST:
      const { jobPost } = action;
      return { ...state, jobPost };

    default:
      return state;
  }
};
