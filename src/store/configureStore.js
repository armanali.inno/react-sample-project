import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from "redux-saga";
import {watcherSaga} from "./sagas/rootSaga";
import userReducer from './reducers';


const reducer = combineReducers({
  user: userReducer
});

const SagaMiddleware = createSagaMiddleware();

const middleware = [SagaMiddleware];

export const store = createStore(reducer, {}, applyMiddleware(...middleware));

SagaMiddleware.run(watcherSaga)

// export default store;
