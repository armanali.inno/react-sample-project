import { takeLatest } from "redux-saga/effects";
import { handleGetUser } from "./handlers";
import { GET_USER } from "../reducers";

export function* watcherSaga() {
  yield takeLatest(GET_USER, handleGetUser);
}
