import { call, put } from "redux-saga/effects";
import { setUser, setJobPost } from "../../reducers";
import { requestGetJobPost} from "../requests";

export function* handleGetUser(action) {
  try {
    const response = yield call(requestGetJobPost);
    const { data } = response;
    yield put(setJobPost(data));
    // yield put(setUser(data));
  } catch (error) {
    console.log(error);

  }
}
